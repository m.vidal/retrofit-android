package mvidal.categorylist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;

import java.io.IOException;

import mvidal.categorylist.Adapters.FeedListAdapter;
import mvidal.categorylist.Interfaces.FeedInterface;
import mvidal.categorylist.Models.ApiResponse;
import mvidal.categorylist.Models.Parsers.Entry;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.List;
import android.widget.ListView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private ListView lst;
    private FeedListAdapter mAdapter;
    private LinearLayout msg_nodata;
    private ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lst = (ListView) findViewById(R.id.lv);
        msg_nodata = (LinearLayout) findViewById(R.id.msg_nodata);

        // ProgressBar
        pb = (ProgressBar) findViewById(R.id.progressBar);

        // Backend Lol
        //callRetrofitChampions();

        // Api Apple
        callRetrofitApple();
    }

    private void callRetrofitApple() {
        pb.setVisibility(View.VISIBLE);

        //Declaramos nuestro objeto Retrofit y le indicamos el endpoint
        //y con que vamos a hacer el parse de la información finalizamos con build
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(FeedInterface.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Declaramos la interfaz y dejamos que retrofit la instancie
        FeedInterface interfaces = retrofit.create(FeedInterface.class);

        //Hacemos el objeto tipo llamada
        Call<ApiResponse> responseCall = interfaces.getApps();

        //Hacemos la llamada asíncrona.
        responseCall.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {

                pb.setVisibility(View.GONE);

                System.out.println("Response url: " + call.request().url());


                // http response status code + headers
                System.out.println("Response status code: " + response.code());

                // isSuccess is true if response code => 200 and <= 300
                if (!response.isSuccessful()) {
                    // print response body if unsuccessful
                    try {
                        System.out.println(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return;
                }


                try {
                    // Revisen en models > parsers > entry
                    ApiResponse feed = response.body();

                    List<Entry> arr_datos = feed.getFeed().getEntry();
                    if(arr_datos.size() > 0) {
                        mAdapter = new FeedListAdapter(MainActivity.this, arr_datos);
                        lst.setAdapter(mAdapter);

                        // Mensaje sin datos
                        msg_nodata.setVisibility(View.GONE);

                        lst.setVisibility(View.VISIBLE);
                    } else {
                        lst.setVisibility(View.GONE);

                        // Mensaje sin datos
                        msg_nodata.setVisibility(View.VISIBLE);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                pb.setVisibility(View.GONE);
                lst.setVisibility(View.GONE);
                msg_nodata.setVisibility(View.VISIBLE);

                Log.e("Error", "pls");
            }
        });
    }
}
