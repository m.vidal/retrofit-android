package mvidal.categorylist.Models.Parsers;

import java.util.HashMap;
import java.util.Map;

import mvidal.categorylist.Models.Parsers.Attributes.AttributeContentType;

/**
 * Created by Manuel Vidal on 12/28/2016.
 */

public class ImContentType {
    private AttributeContentType attributes;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public AttributeContentType getAttributes() {
        return attributes;
    }

    public void setAttributes(AttributeContentType attributes) {
        this.attributes = attributes;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
