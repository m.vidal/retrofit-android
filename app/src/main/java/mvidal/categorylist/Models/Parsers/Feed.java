package mvidal.categorylist.Models.Parsers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mvidal.categorylist.Models.Parsers.Author;
import mvidal.categorylist.Models.Parsers.Entry;
import mvidal.categorylist.Models.Parsers.Icon;
import mvidal.categorylist.Models.Parsers.Id;
import mvidal.categorylist.Models.Parsers.Link;
import mvidal.categorylist.Models.Parsers.Rights;
import mvidal.categorylist.Models.Parsers.Title;
import mvidal.categorylist.Models.Parsers.Updated;

/**
 * Created by Manuel Vidal on 12/28/2016.
 */

public class Feed {
    private Author author;
    private List<Entry> entry = null;
    private Updated updated;
    private Rights rights;
    private Title title;
    private Icon icon;
    private List<Link> link = null;
    private Id id;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Entry> getEntry() {
        return entry;
    }

    public void setEntry(List<Entry> entry) {
        this.entry = entry;
    }

    public Updated getUpdated() {
        return updated;
    }

    public void setUpdated(Updated updated) {
        this.updated = updated;
    }

    public Rights getRights() {
        return rights;
    }

    public void setRights(Rights rights) {
        this.rights = rights;
    }

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    public List<Link> getLink() {
        return link;
    }

    public void setLink(List<Link> link) {
        this.link = link;
    }

    public Id getId() {
        return id;
    }

    public void setId(Id id) {
        this.id = id;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
