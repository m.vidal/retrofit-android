package mvidal.categorylist.Models.Parsers.Attributes;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Manuel Vidal on 12/28/2016.
 */

public class AttributeReleaseDate {
    private String label;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
