package mvidal.categorylist.Models.Parsers;

import java.util.HashMap;
import java.util.Map;

import mvidal.categorylist.Models.Parsers.Attributes.AttributeArtist;

/**
 * Created by Manuel Vidal on 12/28/2016.
 */

public class ImArtist {
    private String label;
    private AttributeArtist attributes;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public AttributeArtist getAttributes() {
        return attributes;
    }

    public void setAttributes(AttributeArtist attributes) {
        this.attributes = attributes;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
