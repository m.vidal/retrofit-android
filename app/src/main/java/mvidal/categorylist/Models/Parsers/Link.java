package mvidal.categorylist.Models.Parsers;

import java.util.HashMap;
import java.util.Map;

import mvidal.categorylist.Models.Parsers.Attributes.AttributeLink;

/**
 * Created by Manuel Vidal on 12/28/2016.
 */

public class Link {
    private AttributeLink attributes;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public AttributeLink getAttributes() {
        return attributes;
    }

    public void setAttributes(AttributeLink attributes) {
        this.attributes = attributes;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
