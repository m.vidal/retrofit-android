package mvidal.categorylist.Models.Parsers;

import java.util.HashMap;
import java.util.Map;

import mvidal.categorylist.Models.Parsers.Attributes.AttributesImage;

/**
 * Created by Manuel Vidal on 12/28/2016.
 */

public class ImImage {
    private String label;
    private AttributesImage attributes;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public AttributesImage getAttributes() {
        return attributes;
    }

    public void setAttributes(AttributesImage attributes) {
        this.attributes = attributes;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
