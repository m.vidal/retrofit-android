package mvidal.categorylist.Interfaces;

import mvidal.categorylist.Models.ApiResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import java.util.List;

/**
 * Created by Manuel Vidal on 12/28/2016.
 */

public interface FeedInterface {

    String URL = "https://itunes.apple.com/";

    @GET("us/rss/topfreeapplications/limit=20/json")
    Call<ApiResponse> getApps();

    @GET("champions")
    Call<List<Champion>> getChamps();

}
