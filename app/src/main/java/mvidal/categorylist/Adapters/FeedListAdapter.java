package mvidal.categorylist.Adapters;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.List;

import mvidal.categorylist.Models.Parsers.Entry;
import mvidal.categorylist.R;

/**
 * Created by Manuel Vidal on 12/28/2016.
 */

public class FeedListAdapter extends BaseAdapter {

    private Activity _activity;
    private List<Entry> _data;
    private static LayoutInflater inflater = null;

    public FeedListAdapter(Activity activity, List<Entry> datos) {
        this._data = datos;
        this._activity = activity;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return _data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        Entry d = _data.get(position);

        vi = inflater.inflate(R.layout.list_item, null);

        TextView name = (TextView) vi.findViewById(R.id.Name);
        name.setText(d.getImName().getLabel());

        TextView category = (TextView) vi.findViewById(R.id.Category);
        category.setText("Category: " + d.getCategory().getAttributes().getLabel());

        return vi;

    }

}
